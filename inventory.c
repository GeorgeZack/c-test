#include <stdio.h>

int main() {
	int a[32];
	enum item { axe, pickaxe, shovel, sword, dirt, stone, torch, apple };
	long long x = 9223372036854775807;
	long long y = 0;
	long long z = 0;
	printf("x: %lld\ny: %lld\nz: %lld\n", x, y, z);
	
	for (size_t i = 0; i < sizeof(a)/sizeof(a[0]); i++) {
		if (a[i] != -1) {
			a[i] = -1;
		}
		
		enum item newItem;
		switch (i) {
			case 0: {
				newItem = sword;
				a[i] = newItem;
				break;
			}
			case 1: {
				newItem = pickaxe;
				a[i] = newItem;
				break;
			}
			case 2: {
				newItem = shovel;
				a[i] = newItem;
				break;
			}
			case 5: {
				newItem = dirt;
				a[i] = newItem;
				break;
			}
		}
		printf("a[%d]: %d\n", i, a[i]);
	}
	
	return 0;
}
