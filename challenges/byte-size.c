#include <stdio.h>

int main() {
	printf("Type        | Size (in bytes)\n");
	printf("------------|----------------\n");
	printf("int         | %zd\n", sizeof(int));
	printf("short       | %zd\n", sizeof(short));
	printf("char        | %zd\n", sizeof(char));
	printf("long        | %zd\n", sizeof(long));
	printf("long long   | %zd\n", sizeof(long long));
	printf("float       | %zd\n", sizeof(float));
	printf("double      | %zd\n", sizeof(double));
	printf("long double | %zd\n\n", sizeof(long double));
	
	return 0;
}
