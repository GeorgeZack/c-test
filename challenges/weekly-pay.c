#include <stdio.h>

#define RATE 12.0
#define RATE_OVERTIME 18.0
#define TAX_BRACKET_FIFTEEN 300
#define TAX_BRACKET_TWENTY 150

int main() {
	double hoursTotal, hoursOvertime, hoursTemp, payOvertime, gross, net, netTemp, taxTotal, taxFifteen, taxTwenty, taxTwentyfive;
	
	printf("Please input hours worked: ");
	scanf("%lf", &hoursTotal);
	
	hoursTemp = hoursTotal;
	if (hoursTotal > 40) {
		hoursOvertime = hoursTemp - 40;
		hoursTemp = hoursTemp - hoursOvertime;
	}
	
	gross = hoursTemp * RATE;
	if (hoursOvertime > 0) {
		payOvertime = hoursOvertime * RATE_OVERTIME;
		gross = gross + payOvertime;
	}
	
	netTemp = gross;
	if (netTemp <= TAX_BRACKET_FIFTEEN) {
		taxFifteen = netTemp;
		netTemp = 0;
	} else if (netTemp > TAX_BRACKET_FIFTEEN && netTemp <= (TAX_BRACKET_FIFTEEN + TAX_BRACKET_TWENTY)) {
		taxFifteen = TAX_BRACKET_FIFTEEN;
		netTemp = netTemp - TAX_BRACKET_FIFTEEN;
		taxTwenty = netTemp;
		netTemp = 0;
	} else if (netTemp > (TAX_BRACKET_FIFTEEN + TAX_BRACKET_TWENTY)) {
		taxFifteen = TAX_BRACKET_FIFTEEN;
		taxTwenty = TAX_BRACKET_TWENTY;
		netTemp = netTemp - TAX_BRACKET_FIFTEEN - TAX_BRACKET_TWENTY;
		taxTwentyfive = netTemp;
		netTemp = 0;
	}
	
	taxTotal = (taxFifteen * 0.15) + (taxTwenty * 0.2) + (taxTwentyfive * 0.25);
	net = gross - taxTotal;
	
	printf("\n-----------PAYSTUB-----------\n");
	printf(" Total hours:      %.2lf\n", hoursTotal);
	printf(" Net pay:          $%.2lf\n", net);
	printf(" Gross pay:        $%.2lf\n", gross);
	printf(" Tax:              $%.2lf\n", taxTotal);
	if (hoursOvertime > 0) {
		printf("Overtime details...\n");
		printf(" Hours:            %.2lf\n", hoursOvertime);
		printf(" Pay:              $%.2lf.\n", payOvertime);
	}
	
	printf("--------------+--------------\n");
	return 0;
}
