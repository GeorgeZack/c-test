#include <stdio.h>

int main(int argc, char *argv[]) {
	// int totalArguments = argc;
	char *arg1 = argv[0];
	char *arg2 = argv[1];
	char *arg3 = argv[2];
	
	printf("arg2: %s\n", arg2);
	printf("arg2: %d\n", *arg2);
	printf("arg2: %f\n", (double) *arg2);
	
	double width = 10.0;
	double height = 20.0;
	double perimeter = 0.0;
	double area = 0.0;
	perimeter = 2.0 * (width + height);
	area = width * height;
	
	printf("Width: 		%f\n", width);
	printf("Height: 	%f\n", height);
	printf("Perimeter: 	%f\n", perimeter);
	printf("Area: 		%f\n", area);
	
	return 0;
}
