#include <stdio.h>

int main() {
	double minutes, hours, days, years;
	
	printf("Please input minutes: ");
	scanf("%lf", &minutes);
	printf("Converting %lf minutes to... ", minutes);
	
	hours = minutes / 60;
	days = hours / 24;
	years = days / 365;
	
	printf("%lf hours, ", hours);
	printf("%lf days, ", days);
	printf("and %lf years.\n", years);
	
	return 0;
}
